<?php

namespace Isotopes\Profiler;

use Illuminate\Support\Arr;
use Jenssegers\Mongodb\Eloquent\Model;

class FormatModel
{
    /**
     * Format the given model to a readable string.
     *
     * @param  Model  $model
     * @return string
     */
    public static function given($model)
    {
        return get_class($model).':'.implode('_', Arr::wrap($model->getKey()));
    }
}
