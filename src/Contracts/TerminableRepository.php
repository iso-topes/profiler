<?php

namespace Isotopes\Profiler\Contracts;

interface TerminableRepository
{
    /**
     * Perform any clean-up tasks needed after storing profiler entries.
     *
     * @return void
     */
    public function terminate();
}