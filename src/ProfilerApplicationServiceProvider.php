<?php

namespace Isotopes\Profiler;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class ProfilerApplicationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->authorization();
    }

    /**
     * Configure the Profiler authorization services.
     *
     * @return void
     */
    protected function authorization()
    {
        $this->gate();

        Profiler::auth(function ($request) {
            return app()->environment('local') ||
                   Gate::check('viewProfiler', [$request->user()]);
        });
    }

    /**
     * Register the Profiler gate.
     *
     * This gate determines who can access Profiler in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewProfiler', function ($user) {
            return in_array($user->email, [
                //
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
