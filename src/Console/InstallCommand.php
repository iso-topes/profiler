<?php

namespace Isotopes\Profiler\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profiler:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install resources of the profiler.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->comment('Publishing Profiler Service Provider...');
        $this->callSilent('vendor:publish', ['--tag' => 'profiler-provider']);

        $this->comment('Publishing Profiler Assets...');
        $this->callSilent('vendor:publish', ['--tag' => 'profiler-assets']);

        $this->comment('Publishing Profiler Configuration...');
        $this->callSilent('vendor:publish', ['--tag' => 'profiler-config']);

        $this->registerServiceProvider();

        $this->info('Profiler scaffolding installed successfully.');
    }

    /**
     *
     */
    protected function registerServiceProvider()
    {
        $namespace = Str::replaceLast('\\', '', $this->laravel->getNamespace());

        $config = file_get_contents(config_path('app.php'));

        if (Str::contains($config, $namespace.'\\Providers\\ProfilerServiceProvider::class')) {
            return;
        }

        $lineEndingCount = [
            "\r\n" => substr_count($config, "\r\n"),
            "\r" => substr_count($config, "\r"),
            "\n" => substr_count($config, "\n"),
        ];

        $eol = array_keys($lineEndingCount, max($lineEndingCount))[0];

        file_put_contents(config_path('app.php'), str_replace(
            "{$namespace}\\Providers\RouteServiceProvider::class,".$eol,
            "{$namespace}\\Providers\RouteServiceProvider::class,".$eol."        {$namespace}\Providers\ProfilerServiceProvider::class,".$eol,
            $config
        ));

        file_put_contents(app_path('Providers/ProfilerServiceProvider.php'), str_replace(
            "namespace App\Providers;",
            "namespace {$namespace}\Providers;",
            file_get_contents(app_path('Providers/ProfilerServiceProvider.php'))
        ));
    }
}
