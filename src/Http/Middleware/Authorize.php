<?php

namespace Isotopes\Profiler\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Isotopes\Profiler\Profiler;

class Authorize
{
    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return Response
     */
    public function handle($request, $next)
    {
        return Profiler::check($request) ? $next($request) : abort(403);
    }
}
