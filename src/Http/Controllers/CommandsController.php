<?php

namespace Isotopes\Profiler\Http\Controllers;

use Isotopes\Profiler\EntryType;
use Isotopes\Profiler\Watchers\CommandWatcher;

class CommandsController extends ProfilerController
{
    /**
     * The entry type for the controller.
     *
     * @return string
     */
    protected function entryType()
    {
        return EntryType::COMMAND;
    }

    /**
     * The watcher class for the controller.
     *
     * @return string
     */
    protected function watcher()
    {
        return CommandWatcher::class;
    }
}
