<?php

namespace Isotopes\Profiler\Http\Controllers;

use Isotopes\Profiler\EntryType;
use Isotopes\Profiler\Watchers\ModelWatcher;

class ModelsController extends ProfilerController
{
    /**
     * The entry type for the controller.
     *
     * @return string
     */
    protected function entryType()
    {
        return EntryType::MODEL;
    }

    /**
     * The watcher class for the controller.
     *
     * @return string
     */
    protected function watcher()
    {
        return ModelWatcher::class;
    }
}
