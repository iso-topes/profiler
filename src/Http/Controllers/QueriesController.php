<?php

namespace Isotopes\Profiler\Http\Controllers;

use Isotopes\Profiler\EntryType;
use Isotopes\Profiler\Watchers\QueryWatcher;

class QueriesController extends ProfilerController
{
    /**
     * The entry type for the controller.
     *
     * @return string
     */
    protected function entryType()
    {
        return EntryType::QUERY;
    }

    /**
     * The watcher class for the controller.
     *
     * @return string
     */
    protected function watcher()
    {
        return QueryWatcher::class;
    }
}
