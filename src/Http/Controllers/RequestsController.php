<?php

namespace Isotopes\Profiler\Http\Controllers;

use Isotopes\Profiler\EntryType;
use Isotopes\Profiler\Watchers\RequestWatcher;

class RequestsController extends ProfilerController
{
    /**
     * The entry type for the controller.
     *
     * @return string
     */
    protected function entryType()
    {
        return EntryType::REQUEST;
    }

    /**
     * The watcher class for the controller.
     *
     * @return string
     */
    protected function watcher()
    {
        return RequestWatcher::class;
    }
}
