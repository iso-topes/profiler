<?php

use Illuminate\Database\Eloquent\Factory;
use Isotopes\Profiler\EntryType;
use Isotopes\Profiler\Storage\ProfilerModel;

/**
 * @var Factory $factory
 */
$factory->define(ProfilerModel::class, function (Faker\Generator $faker) {
    return [
        'sequence' => random_int(1, 10000),
        'uuid' => $faker->uuid,
        'batch_id' => $faker->uuid,
        'type' => $faker->randomElement([
            EntryType::CACHE, EntryType::COMMAND, EntryType::DUMP, EntryType::EVENT, EntryType::EXCEPTION,
            EntryType::JOB, EntryType::LOG, EntryType::MAIL, EntryType::MODEL, EntryType::NOTIFICATION,
            EntryType::QUERY, EntryType::REDIS, EntryType::REQUEST, EntryType::SCHEDULED_TASK,
        ]),
        'content' => [$faker->word => $faker->word],
    ];
});
